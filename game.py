from pygame import *


class Sprite:
    def __init__(self, x_pos, y_pos, filename):
        self.x = x_pos
        self.y = y_pos
        self.bitmap = image.load(filename)
        self.bitmap.set_colorkey((0, 0, 0))

    def set_position(self, x_pos, y_pos):
        self.x = x_pos
        self.y = y_pos

    def render(self):
        screen.blit(self.bitmap, (self.x, self.y))


def detect_collision(x1, y1, x2, y2):
    if (x1 > x2 - 25) and (x1 < x2 + 50) and (y1 > y2 - 26) and (y1 < y2 + 26):
        return 1
    else:
        return 0


# Necessary PyGame config
init()
screen = display.set_mode((640, 480))
key.set_repeat(1, 1)
display.set_caption('supInvader')
backdrop = image.load('data/background.jpg')

# Generate enemies
enemies = []
for count in range(10):
    enemies.append(Sprite(50 * count + 50, 50, 'data/enemy.png'))
    count += 1

# Set image for ship
ship = Sprite(20, 400, 'data/ship.png')
our_missile = Sprite(0, 480, 'data/laser.png')

# Quit the game or not
exit = 0

# Speed settings
enemy_speed = 3
missile_speed = 5

# Loop game
while exit == 0:
    # Set background image on the board game
    screen.blit(backdrop, (0, 0))

    for count in range(len(enemies)):
        # Move our enemy
        # enemies[count].x += enemy_speed
        # Display our enemy
        enemies[count].render()

    # If the ship arrive at the end of the screen
    if enemies[len(enemies) - 1].x > 590:
        enemy_speed = -3

    # If the ship arrive at the beginning of the screen
    if enemies[0].x < 10:
        enemy_speed = 3

    # Check position of the missile and display it
    if 479 > our_missile.y > 0:
        our_missile.render()
        our_missile.y -= missile_speed

    # Check collision between missile and enemy
    for count in range(0, len(enemies)):
        if detect_collision(our_missile.x, our_missile.y, enemies[count].x, enemies[count].y):
            # Collision detect we destroy the enemy
            del enemies[count]
            break

    # No more enemy
    if len(enemies) == 0:
        exit = 1

    for our_event in event.get():
        if our_event.type == QUIT:
            exit = 1
        if our_event.type == KEYDOWN:
            if our_event.key == K_RIGHT and ship.x < 590:
                ship.x += 5
            if our_event.key == K_LEFT and ship.x > 10:
                ship.x -= 5
            if our_event.key == K_SPACE:
                our_missile.x = ship.x + 23
                our_missile.y = ship.y

    # Display the ship
    ship.render()
    # Refresh the screen
    display.update()
    time.delay(5)
